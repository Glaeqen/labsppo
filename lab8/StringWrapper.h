#pragma once

class StringWrapper {
	//Private fields:
	char *_string;	
	int _stringLength;

	//Private methods:
	//Reallocates memory for string with new 'newStringLength' volume
	//and copies characters from old to new array.
	void reallocMemory(int newStringLength);
public:
	//Constructors:
	//Creates an object and initializes the string. 
	StringWrapper(const char *string);
	//Copying constructor:
	StringWrapper(const StringWrapper& stringWrapper);

	//Static methods:
	//Checks if strings from stringWrappers' objects are equal.
	static bool eq(const StringWrapper& stringWrapper1, const StringWrapper& stringWrapper2);
	//Checks if strings from stringWrappers' objects are equal. Ignores cases.
	static bool eqIcase(const StringWrapper& stringWrapper1, const StringWrapper& stringWrapper2);

	//Non-const methods:
	//Appends string to _string.
	StringWrapper& append(const char *string);
	//Appends string from stringWrapper object to _string.
	StringWrapper& append(const StringWrapper& stringWrapper);

	//Const methods:
	//Extracts substring from _string from 'start' to 'end' character.
	StringWrapper substring(int start, int end) const;
	//Getters
	//Returns _string.
	const char *data() const;
	//Returns _stringLength. 
	int getStringLength() const;

	//Destructor
	//Frees memory allocated for _string.
	~StringWrapper();
};

//Global functions:
//Prints string to stdout.
void print(const char *string);
//Prints string from stringWrapper object to stdout.
void print(const StringWrapper& stringWrapper);