#include <iostream>
#include <cstring>
#include "StringWrapper.h"

using std::cout;
using std::endl;

void StringWrapper::reallocMemory(int newStringLength){
	char *newString = new char[newStringLength];
	
	strcpy(newString, _string);
	delete[] _string;

	_string = newString;
	_stringLength = newStringLength;
}

StringWrapper::StringWrapper(const char *string){
	int stringLength = strlen(string) + 1;

	_string = new char[stringLength];

	strcpy(_string, string);
	_stringLength = stringLength;
}

StringWrapper::StringWrapper(const StringWrapper& stringWrapper){
	_stringLength = stringWrapper.getStringLength();
	_string = new char[_stringLength];
	strcpy(_string, stringWrapper.data());
}

bool StringWrapper::eq(const StringWrapper& stringWrapper1, const StringWrapper& stringWrapper2){
	return !strcmp(stringWrapper1.data(), stringWrapper2.data());
}

bool StringWrapper::eqIcase(const StringWrapper& stringWrapper1, const StringWrapper& stringWrapper2){
	return !strcasecmp(stringWrapper1.data(), stringWrapper2.data());
}

StringWrapper& StringWrapper::append(const char *string){
	int newStringLength = _stringLength + strlen(string);
	reallocMemory(newStringLength);
	strcat(_string, string);

	return *this;
}

StringWrapper& StringWrapper::append(const StringWrapper& stringWrapper){
	int newStringLength = _stringLength + stringWrapper.getStringLength() - 1;
	reallocMemory(newStringLength);
	strcat(_string, stringWrapper.data());

	return *this;
}

StringWrapper StringWrapper::substring(int start, int end) const{
	char *newString = new char[end-start+1];

	strncpy(newString, _string+start, end-start-1);
	newString[end-start-1] = 0;

	StringWrapper stringWrapper(newString);
	
	delete[] newString;

	return stringWrapper;
}

const char *StringWrapper::data() const{
	return _string;
}

int StringWrapper::getStringLength() const{
	return _stringLength;
}

StringWrapper::~StringWrapper(){
	delete[] _string;
	_string = NULL;
	_stringLength = 0;
}

void print(const char *string){
	cout << string << endl;
}

void print(const StringWrapper& stringWrapper){
	cout << stringWrapper.data() << endl;
}