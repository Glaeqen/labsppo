#pragma once

#include "StringWrapper.h"
class Wykres{
public:
	//Class "Punkt" which takes care of single position 'x,y'
	class Punkt{
		float x_;
		float y_;
	public:
		//Public constructor:
		//NULLs private fields
		Punkt();
		//Setter:
		void setValues(float x, float y);

		//Getters:
		float x() const;
		float y() const;
	};
	//Public constructors:
	//Creates object type Wykres and initialises it with arguments given
	Wykres(const char *title, const char *axisNameX, const char *axisNameY);
	//Copying constructor
	Wykres(const Wykres& wykres);

	//Non-const methods:
	//Adds point to the and of array
	void add(float x, float y);
	//Removes point from array at given index
	void usun(int index);
	//Returns reference to title_ field and allows modifying it
	StringWrapper& tytul();

	//Const methods:
	//Prints Wykres fields in an appropriate manner
	void print() const;

	//Returns new Point with average values of points in pointsArray_
	Wykres::Punkt srednia() const;

	//Destructor:
	//Frees allocated memory for Punkt class
	~Wykres();

private:
	//Private fields:
	StringWrapper title_;
	StringWrapper axisNameX_;
	StringWrapper axisNameY_;
	Wykres::Punkt *pointsArray_;
	int pointsArraySize_;
};