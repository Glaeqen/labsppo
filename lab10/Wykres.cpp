#include <iostream>

#include "Wykres.h"

using std::cout;
using std::endl;


Wykres::Punkt::Punkt():
x_(0),
y_(0)
{}

void Wykres::Punkt::setValues(float x, float y){
	x_ = x;
	y_ = y;
}

float Wykres::Punkt::x() const{
	return x_;
}

float Wykres::Punkt::y() const{
	return y_;
}

Wykres::Wykres(const char *title, const char *axisNameX, const char *axisNameY):
title_(title),
axisNameX_(axisNameX),
axisNameY_(axisNameY),
pointsArray_(NULL),
pointsArraySize_(0)
{}

Wykres::Wykres(const Wykres& wykres):
title_(wykres.title_),
axisNameX_(wykres.axisNameX_),
axisNameY_(wykres.axisNameY_)
{
	pointsArraySize_ = wykres.pointsArraySize_;
	pointsArray_ = new Punkt[pointsArraySize_];
	for(int i=0; i<pointsArraySize_; i++){
		pointsArray_[i] = wykres.pointsArray_[i]; 
	}
}

void Wykres::add(float x, float y){
	//Increase array size
	Wykres::Punkt *newpointsArray = new Wykres::Punkt[pointsArraySize_+1];
	for(int i=0; i<pointsArraySize_; i++){
		newpointsArray[i] = pointsArray_[i];
	}
	if(pointsArraySize_) delete[] pointsArray_;
	pointsArray_ = newpointsArray;
	pointsArraySize_++;

	//Set appropriate values
	pointsArray_[pointsArraySize_-1].setValues(x, y);
}

void Wykres::usun(int index){
	if(index>=pointsArraySize_) return;

	Wykres::Punkt *newpointsArray = new Wykres::Punkt[pointsArraySize_-1];
	for(int i=0; i<index; i++){
		newpointsArray[i] = pointsArray_[i];
	}
	for(int i=index+1; i<pointsArraySize_; i++){
		newpointsArray[i-1] = pointsArray_[i];
	}

	delete[] pointsArray_;
	pointsArray_ = newpointsArray;
	pointsArraySize_--;
}

StringWrapper& Wykres::tytul() {
	return title_;
}

void Wykres::print() const{
	cout << title_.data() << " x:" << axisNameX_.data() << " y:" << axisNameY_.data() << endl;
	for(int i=0; i<pointsArraySize_; i++){
		cout<< pointsArray_[i].x() << " " << pointsArray_[i].y() << endl;
	}
}

Wykres::Punkt Wykres::srednia() const{
	Punkt punkt;
	float avrX = 0;
	float avrY = 0;

	for(int i=0; i<pointsArraySize_; i++){
		avrX += pointsArray_[i].x();
		avrY += pointsArray_[i].y();
	}

	avrX /= pointsArraySize_;
	avrY /= pointsArraySize_;

	punkt.setValues(avrX, avrY);

	return punkt;
}

Wykres::~Wykres(){
	cout << "~Wykres " << title_.data() << endl;
	
	delete [] pointsArray_;
	pointsArray_ = NULL;
	pointsArraySize_ = 0;
}