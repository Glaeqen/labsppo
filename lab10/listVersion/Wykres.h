#pragma once

#include "StringWrapper.h"

#ifndef NULL
#define NULL 0
#endif

class Wykres{
public:
	class Punkt{
		float x_;
		float y_;
		Wykres::Punkt *next_;
	public:
		Punkt(float x, float y, Wykres::Punkt *next = NULL);
		Punkt(const Punkt& punkt);

		void setNext(Wykres::Punkt *next);

		float x() const;
		float y() const;
		Wykres::Punkt *getNext() const;

		~Punkt();
	};

	Wykres(const char *title, const char *axisX, const char *axisY);
	Wykres(const Wykres& wykres);

	StringWrapper& tytul();
	void add(float x, float y);
	void usun(int index);

	void print() const;
	Wykres::Punkt srednia() const;

	~Wykres();
private:
	StringWrapper title_;
	StringWrapper axisX_;
	StringWrapper axisY_;

	Punkt *head_;
	Punkt *tail_;
};