#include "Wykres.h"

Wykres::Punkt::Punkt(float x, float y, Wykres::Punkt *next):
	x_(x),
	y_(y),
	next_(next)
{}

Wykres::Punkt::Punkt(const Punkt& punkt):
	x_(punkt.x_),
	y_(punkt.y_),
	next_(NULL)
{
	if(!punkt.next_)	return;
	next_ = new Wykres::Punkt(*(punkt.next_));
}

void Wykres::Punkt::setNext(Wykres::Punkt *next){
	next_ = next;
}

float Wykres::Punkt::x() const{
	return x_;
}

float Wykres::Punkt::y() const{
	return y_;
}

Wykres::Punkt *Wykres::Punkt::getNext() const{
	return next_;
}

Wykres::Punkt::~Punkt(){
	if(!next_)	return;

	delete next_;
	next_ = NULL;
}