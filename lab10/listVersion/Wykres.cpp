#include <iostream>

#include "Wykres.h"

using std::cout;
using std::endl;


Wykres::Wykres(const char *title, const char *axisX, const char *axisY):
	title_(title),
	axisX_(axisX),
	axisY_(axisY),
	head_(NULL),
	tail_(NULL)
{}

Wykres::Wykres(const Wykres& wykres):
	title_(wykres.title_),
	axisX_(wykres.axisX_),
	axisY_(wykres.axisY_),
	head_(NULL),
	tail_(NULL)
{	
	if(wykres.head_ == NULL)	return;

	head_= new Wykres::Punkt(*(wykres.head_));
	
	Wykres::Punkt *tempPunkt;
	for(tempPunkt = head_; tempPunkt->getNext() != NULL; tempPunkt = tempPunkt->getNext());

	tail_ = tempPunkt;
}

StringWrapper& Wykres::tytul(){
	return title_;
}

void Wykres::add(float x, float y){
	if(head_ == NULL){
		head_ = tail_ = new Punkt(x, y);
		return;
	}

	tail_->setNext(new Punkt(x, y));
	tail_ = tail_->getNext();
}

void Wykres::usun(int index){
	if(!head_)	return;

	Wykres::Punkt *tempPunkt = head_;

	if(!index){
		head_ = head_->getNext();
		tempPunkt->setNext(NULL);
		delete tempPunkt;
		return;
	}

	for(int i=0; i<index-1; i++, tempPunkt = tempPunkt->getNext());	//tempPunkt->next to remove

	if(!tempPunkt->getNext())	return;

	Wykres::Punkt *restListHolder = tempPunkt->getNext()->getNext();

	tempPunkt->getNext()->setNext(NULL);
	delete tempPunkt->getNext();
	tempPunkt->setNext(restListHolder);
	if(!restListHolder)	tail_ = tempPunkt;
}

void Wykres::print() const{
	cout << title_.data() << " x:" << axisX_.data() << " y:" << axisY_.data() << endl;
	for(Wykres::Punkt *tempPunkt = head_; tempPunkt != NULL; tempPunkt = tempPunkt->getNext()){
		cout << tempPunkt->x() << " " << tempPunkt->y() << endl;
	}
}

Wykres::Punkt Wykres::srednia() const{
	float avrX = 0;
	float avrY = 0;
	int listSize = 0;

	for(Wykres::Punkt *tempPunkt = head_; tempPunkt != NULL; tempPunkt = tempPunkt->getNext(), listSize++){
		avrX += tempPunkt->x();
		avrY += tempPunkt->y();
	}

	if(!listSize)	return Wykres::Punkt(0, 0);

	avrX /= listSize;
	avrY /= listSize;

	return Wykres::Punkt(avrX, avrY);
}

Wykres::~Wykres(){
	cout << "~Wykres " << title_.data() << endl;
	if(!head_)	return;
	delete head_;
}
