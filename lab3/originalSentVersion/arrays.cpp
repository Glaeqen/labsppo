#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "Lab02.h"

void allocMemory(int numberOfStudents, char ***namesList, char ***surnamesList, int **yearsList, const char *firstName, const char *secondName, char *surname, int *namesLength){
	if(!numberOfStudents){
		*namesList = (char **)malloc(sizeof(**namesList)*(numberOfStudents+1));
		*surnamesList = (char **)malloc(sizeof(**surnamesList)*(numberOfStudents+1));
		*yearsList = (int *)malloc(sizeof(**yearsList)*(numberOfStudents+1));
	}
	else{
	*namesList = (char **)realloc(*namesList, sizeof(**namesList)*(numberOfStudents+1));
	*surnamesList = (char **)realloc(*surnamesList, sizeof(**surnamesList)*(numberOfStudents+1));
	*yearsList = (int *)realloc(*yearsList, sizeof(**yearsList)*(numberOfStudents+1));
	}

	*namesLength = strlen(firstName) + strlen(secondName) + 2;
	int surnameLength = strlen(surname) + 1;

	(*namesList)[numberOfStudents] = (char *)malloc(sizeof(***namesList)*(*namesLength));
	(*surnamesList)[numberOfStudents] = (char *)malloc(sizeof(***surnamesList)*surnameLength);
}


void AddStudent(int *numberOfStudents, char ***namesList, char ***surnamesList, int **yearsList, const char *firstName, const char *secondName, char *surname, int year){
	int namesLength = 0;

	allocMemory(*numberOfStudents, namesList, surnamesList, yearsList, firstName, secondName, surname, &namesLength);

	char tempString[namesLength] = {0};
	sprintf(tempString, "%s %s", firstName, secondName);
	strcpy((*namesList)[*numberOfStudents], tempString);
	strcpy((*surnamesList)[*numberOfStudents], surname);

	(*yearsList)[*numberOfStudents] = year;
	(*numberOfStudents)++;
}

void PrintListContent(int numberOfStudents, char **list){
	for(int i=0; i<numberOfStudents; i++){
		printf("%s\n", list[i]);
	}
}

void PrintListContent(int numberOfStudents, int *yearsList){
	for(int i=0; i<numberOfStudents; i++){
		printf("%d\n", yearsList[i]);
	}
}

void PrintListContent(int numberOfStudents, char **namesList, char **surnamesList, int *yearsList){
	for(int i=0; i<numberOfStudents; i++){
		printf("%s, %s - year %d\n", surnamesList[i], namesList[i], yearsList[i]);
	}
}

void ClearStudents(int *numberOfStudents, char ***namesList, char ***surnamesList, int **yearsList){
	for(int i=0; i<*numberOfStudents; i++){
		free((*namesList)[i]);
		free((*surnamesList)[i]);
	}
	free(*namesList);
	free(*surnamesList);
	free(*yearsList);
}


