#ifndef _LAB_
#define _LAB_

void allocMemory(int numberOfStudents, char ***namesList, char ***surnamesList, int **yearsList, const char *firstName, const char *secondName, char *surname, int *namesLength);
void AddStudent(int *numberOfStudents, char ***namesList, char ***surnamesList, int **yearsList, const char *firstName, const char *secondName, char *surname, int year);
void PrintListContent(int numberOfStudents, char **list);
void PrintListContent(int numberOfStudents, int *yearsList);
void PrintListContent(int numberOfStudents, char **namesList, char **surnamesList, int *yearsList);
void ClearStudents(int *numberOfStudents, char ***namesList, char ***surnamesList, int **yearsList);

#endif