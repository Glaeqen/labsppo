#ifndef _LIST_
#define _LIST_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct tagListNode{
	char data[100];
	struct tagListNode *next;
}ListNode;

typedef struct tagList{
	ListNode *head;
}List;

void prepare(List *);
void add(List *, char *);
void dump(const List *);
int empty(const List*);
void clear(List *);

#endif
