#include "List.h"
void prepare(List *list){
	list->head = NULL;
}

void add(List *list, char *string){
	if(list->head==NULL){
		list->head = (ListNode*)malloc(sizeof(*(list->head)));
		strcpy(list->head->data, string);
		list->head->next = NULL;
		return;
	}
	ListNode *temp = list->head;
	for(;temp->next; temp=temp->next);
	temp->next = (ListNode*)malloc(sizeof(*(temp->next)));
  strcpy(temp->next->data, string);
	temp->next->next = NULL;
}

void dump(const List *list){
	ListNode *temp = list->head;
	for(;temp; temp=temp->next){
		printf("%s ", temp->data);
	}
	putchar('\n');
}

int empty(const List* list){
	if(list->head==NULL)	return 1;
	return 0;
}

void clear(List *list){
	ListNode *tempHead = list->head;
	while(list->head!=NULL){
		tempHead = list->head;
		list->head = list->head->next;
		free(tempHead);
	}
}


