#pragma once

struct MapPoint{
	char *name;
	double lat;
	double lon;
};

struct MapDist{
	double latitude;
	double longitude;
};

// Makes a new MapPoint, fills it with data and returns it.
MapPoint construct(const char *name, const double lat, const double lon);
// Makes a new MapPoint which is distanceLat and distanceLon away from mapPoint and returns it.
MapPoint construct(const MapPoint mapPoint, const double distanceLat, const double distanceLon);

// Prints informations about mapPoint (name, lat and lon).
void print(const MapPoint mapPoint);

// Makes MapDist which containes distance between points mapPoint1 and mapPoint2.
MapDist distance(const MapPoint mapPoint1, const MapPoint mapPoint2);
// Returns distance based on mapDist object using Pythagoras Theorem.
double angularDistance(const MapDist mapDist);

// Returns MapPoint (mapPoint1 or mapPoint2) which is the closest from mapPoint.
MapPoint closestFrom(const MapPoint mapPoint, const MapPoint mapPoint1, const MapPoint mapPoint2);

// Returns name of mapPoint.
char *name(const MapPoint mapPoint);

// Makes a new MapPoint which stands in the middle betweem mapPoint1 and mapPoint2 and gives it a newName.
MapPoint inTheMiddle(const MapPoint mapPoint1, const MapPoint mapPoint2, const char *newName);

// Clears a memory.
void clear(MapPoint& mapPoint);
void clear(MapPoint& mapPoint1, MapPoint& mapPoint2);
void clear(MapPoint& mapPoint1, MapPoint& mapPoint2, MapPoint& mapPoint3);
