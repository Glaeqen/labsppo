#include <cstring>
#include <cstdio>
#include <iostream>
#include <math.h>

#include "MapPoint.h"

MapPoint construct(const char *name, const double lat, const double lon){
	int nameLength = strlen(name) + 1;
	MapPoint newMapPoint;
	newMapPoint.name = new char[nameLength];

	strcpy(newMapPoint.name, name);
	newMapPoint.lat = lat;
	newMapPoint.lon = lon;

	return newMapPoint;
}

MapPoint construct(const MapPoint mapPoint, const double distanceLat, const double distanceLon){
	double lat = mapPoint.lat + distanceLat;
	double lon = mapPoint.lon + distanceLon;
	char newName[100] = {0};

	sprintf(newName, "Nearby %s", mapPoint.name);

	return construct(newName, lat, lon);
}

void print(const MapPoint mapPoint){
	char lat = 'N';
	if(mapPoint.lat<0)	lat = 'S';
	char lon = 'E';
	if(mapPoint.lon<0)	lon = 'W';

	std::cout << mapPoint.name << " ";
	std::cout << fabs(mapPoint.lat) << lat << " ";
	std::cout << fabs(mapPoint.lon) << lon << std::endl;
}

MapDist distance(const MapPoint mapPoint1, const MapPoint mapPoint2){
	MapDist newMapDist;

	newMapDist.latitude = mapPoint1.lat - mapPoint2.lat;
	newMapDist.longitude = mapPoint1.lon - mapPoint2.lon;

	return newMapDist;
}

double angularDistance(const MapDist mapDist){
	double distance = sqrt(pow(mapDist.latitude, 2) + pow(mapDist.longitude, 2));
	
	return distance;
}

MapPoint closestFrom(const MapPoint mapPoint, const MapPoint mapPoint1, const MapPoint mapPoint2){
	if(angularDistance(distance(mapPoint, mapPoint1)) < angularDistance(distance(mapPoint, mapPoint2)))	return mapPoint1;
	else	return mapPoint2;
}

char *name(const MapPoint mapPoint){
	return mapPoint.name;
}

MapPoint inTheMiddle(const MapPoint mapPoint1, const MapPoint mapPoint2, const char *newName){
	double lat = (mapPoint1.lat + mapPoint2.lat)/2;
	double lon = (mapPoint1.lon + mapPoint2.lon)/2;
	
	return construct(newName, lat, lon);
}

void clear(MapPoint& mapPoint){
	delete[] mapPoint.name;
}

void clear(MapPoint& mapPoint1, MapPoint& mapPoint2){
	clear(mapPoint1), clear(mapPoint2);
}

void clear(MapPoint& mapPoint1, MapPoint& mapPoint2, MapPoint& mapPoint3){
	clear(mapPoint1), clear(mapPoint2), clear(mapPoint3);
}