#ifndef Students_h
#define Students_h

class Student;

class Students
{	
	//Private fields
	//1D array of Student pointers
	Student** _studentArray;
	//Size of _studentArray
	int _size;

	//Private methods
	//Increases arrays of Student's pointer by one. 
	//Returns pointer to new array.
	Student** increaseArraySize(Student** source);
public:
	//Public constructors
	//NULLs _studentArray when objects is created.
	Students();

	//Public methods
	//Add new student (pointer) to the array.
	void add(Student* student);
	void add(const Student& student);
	//Printing methods
	void printNames()const;
	void printSurnames()const;
	void printAll()const;
	//Returns student's pointer at specified array index.
	Student* at(int index)const;
	//Clears array, every its element and prepares object for new usage.
	void clear();
};

#endif