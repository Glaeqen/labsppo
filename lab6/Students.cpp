#include <iostream>
#include "Student.h"
#include "Students.h"

using std::cout;
using std::endl;

//Private methods
/////////////////////////////////////////////////////////

Student** Students::increaseArraySize(Student** source)
{	
	_size++;
	Student** newStudentArray = new Student*[_size];

	if(source == NULL){
		return newStudentArray;
	}

	for(int i=0; i<_size-1; ++i)
	{
		newStudentArray[i] = source[i];
	}

	delete[] _studentArray;

	return newStudentArray;
}

//Public constructors
/////////////////////////////////////////////////////////

Students::Students()
{
	_studentArray = NULL;
	_size = 0;	
}


//Public methods
/////////////////////////////////////////////////////////

void Students::add(Student* student)
{
	Student** newStudentArray = increaseArraySize(_studentArray);

	newStudentArray[_size-1] = student;

	_studentArray = newStudentArray;
}

void Students::add(const Student& student)
{
	Student *newStudent = new Student(student.name1(), student.name2(), student.surname(), student.year());
	add(newStudent);
}

void Students::printNames()const
{
	for(int i=0; i<_size; ++i){
		cout << _studentArray[i]->name1() << " " << _studentArray[i]->name2() << endl;
	}
}

void Students::printSurnames()const
{
	for(int i=0; i<_size; ++i){
		cout << _studentArray[i]->surname() << endl;
	}
}

void Students::printAll()const
{
	for(int i=0; i<_size; ++i){
		cout << _studentArray[i]->name1() << " " << _studentArray[i]->name2() << " ";
		cout << _studentArray[i]->surname() << " " << _studentArray[i]->year() << endl;
	}
}

Student* Students::at(int index)const{
	return _studentArray[index];
}

void Students::clear(){
	for(int i=0; i<_size; ++i){
		delete _studentArray[i];
	}
	delete[] _studentArray;

	_studentArray = NULL;
	_size = 0;
}