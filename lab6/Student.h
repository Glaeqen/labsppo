#ifndef Student_h
#define Student_h

class Student
{
	//Private fields
	char* _name1;
	char* _name2;
	char* _surname;
	int _year;
	
	//Private constructors >> Invalid way to create an object.
	Student();
public:
	//Public constructors
	//Creates student and initializes object's fields.
	Student(const char* name1, const char* name2, const char* surname, int year);

	//Getters
	char* name1()const;
	char* name2()const;
	char* surname()const;
	int year()const;
	//Destructor
	//Deallocates strings when object is being destroyed.
	~Student();
};

#endif