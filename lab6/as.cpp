/* Celem zadania jest przepisanie zadania z tablica studentow z uzyciem klas.
   
   UWAGA: Klasa Students przechowuje obiekty zaalokowane na stosie i odpowiada za ich dealokacje.
   UWAGA: Mozna sobie uproscic zarzadzanie pamiecia na napisy przy pomocy tablicy o ustalonym z gory rozmiarze.

 */


#include <iostream>
#include <string.h>
#include "Student.h"
#include "Student.h"
#include "Students.h"
#include "Students.h"

int main ()
{
  using namespace std;
  Students s;
  char su[200];
  strcpy(su, "Spiegel");
  s.add( new Student("Spike", "Jozef", su, 3) );
  s.add( new Student("Rick", "Albert", "Alvarezi", 7) );
  s.add( Student("Roderick", "Pean", "Render", 7) ); // trzeba zapewnic odpowiednia kopiowalnosc w kl. Student
  
  const Students& cs = s;

  cs.printNames();
  cs.printSurnames();
  cs.printAll();

  std::cout << cs.at(0)->name1() << " " << cs.at(0)->name2() << " " << cs.at(0)->surname() << " " << cs.at(0)->year() << std::endl;
  const Student* one = cs.at(2);
  std::cout << one->surname() << std::endl;  
  s.clear();
  s.add(new Student("John", "Roger", "Bean", 12));				 
  cs.printAll();
  s.clear();
  return 0;
}
/* wynik dzialania programu:
Spike Jozef
Rick Albert
Roderick Pean
Spiegel
Alvarezi
Render
Spike Jozef Spiegel 3
Rick Albert Alvarezi 7
Roderick Pean Render 7
Spike Jozef Spiegel 3
Render
John Roger Bean 12
*/
