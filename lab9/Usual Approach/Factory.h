#pragma once

class TestObj{
	char *string_;
public:
	TestObj();
	TestObj(const char *string);
	TestObj(const TestObj& testObj);

	const char *name() const;

	~TestObj();
};

class Factory{
	static TestObj **array_;
	static TestObj *prototypePtr;
	static int arraySize_;
	static void increaseArraySize();
public:
	static TestObj *produce();
	static TestObj *prototype(const TestObj& testObj);
	static void erase();
};