#include <iostream>
#include <cstring>

#include "Factory.h"

using std::cout;
using std::endl;

TestObj **Factory::array_ = 0;
TestObj *Factory::prototypePtr = 0;
int Factory::arraySize_ = 0;

TestObj::TestObj(){
	string_ = new char[strlen("domniemany")+1];
	strcpy(string_, "domniemany");

	cout << "TestObj::TestObj z arg char: " << string_ << endl;
}

TestObj::TestObj(const char *string){
	string_ = new char[strlen(string)+1];
	strcpy(string_, string);

	cout << "TestObj::TestObj z arg char: " << string_ << endl;
}

TestObj::TestObj(const TestObj& testObj){
	string_ = new char[strlen(testObj.string_)+1];
	strcpy(string_, testObj.string_);

	cout << "TestObj::TestObj z arg TestObj: " << string_ << endl;
}

const char *TestObj::name() const{
	return string_;
}

TestObj::~TestObj(){
	cout << "TestObj::~TestObj " << string_ << endl;

	delete[] string_;
	string_ = 0;
}

void Factory::increaseArraySize(){
	TestObj **newArray = new TestObj*[arraySize_+1];
	for(int i=0; i<arraySize_; i++){
		newArray[i] = array_[i];
	}
	if(array_)	delete[] array_;
	array_ = newArray;
	arraySize_++;
}

TestObj *Factory::produce(){
	TestObj *newTestObj;
	if(prototypePtr){
		newTestObj = new TestObj(*prototypePtr);
	}
	else{
		newTestObj = new TestObj();
	}
	increaseArraySize();
	array_[arraySize_-1] = newTestObj;
	return newTestObj;
}

TestObj *Factory::prototype(const TestObj& testObj){
	TestObj *newTestObj = new TestObj(testObj);
	prototypePtr = newTestObj;
	return newTestObj;
}

void Factory::erase(){
	if(!array_) return;
	for(int i=0; i<arraySize_; i++){
		delete array_[i];
	}
	delete[] array_;
	delete prototypePtr;
	array_ = 0;
	prototypePtr = 0;
	arraySize_ = 0;
}