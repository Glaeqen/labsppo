#include <iostream>
#include <cstring>

#include "Factory.h"

using std::cout;
using std::endl;

Factory *Factory::instance = 0;

TestObj::TestObj(){
	string_ = new char[strlen("domniemany")+1];
	strcpy(string_, "domniemany");

	cout << "TestObj::TestObj z arg char: " << string_ << endl;
}

TestObj::TestObj(const char *string){
	string_ = new char[strlen(string)+1];
	strcpy(string_, string);

	cout << "TestObj::TestObj z arg char: " << string_ << endl;
}

TestObj::TestObj(const TestObj& testObj){
	string_ = new char[strlen(testObj.string_)+1];
	strcpy(string_, testObj.string_);

	cout << "TestObj::TestObj z arg TestObj: " << string_ << endl;
}

const char *TestObj::name() const{
	return string_;
}

TestObj::~TestObj(){
	cout << "TestObj::~TestObj " << string_ << endl;

	delete[] string_;
	string_ = 0;
}

void Factory::ifNoInstanceCreate(){
	if(!instance){
		instance = new Factory();
	}
}

void Factory::increaseArraySize(){
	TestObj **newArray = new TestObj*[arraySize_+1];

	for(int i=0; i<arraySize_; i++){
		newArray[i] = array_[i];
	}

	if(array_)	delete[] array_;

	array_ = newArray;
	arraySize_++;
}

Factory::Factory()
:	array_(0),
	prototypePtr_(0),
	arraySize_(0)
{}

TestObj *Factory::produce(){
	ifNoInstanceCreate();

	TestObj *newTestObj;
	if(instance->prototypePtr_){
		newTestObj = new TestObj(*(instance->prototypePtr_));
	}
	else{
		newTestObj = new TestObj();
	}

	instance->increaseArraySize();
	instance->array_[instance->arraySize_-1] = newTestObj;
	return newTestObj;
}

TestObj *Factory::prototype(const TestObj& testObj){
	ifNoInstanceCreate();

	TestObj *newTestObj = new TestObj(testObj);
	instance->prototypePtr_ = newTestObj;
	return newTestObj;
}

void Factory::erase(){
	if(!instance)	return;
	instance->~Factory();
	delete instance;

	instance = 0;
}

Factory::~Factory(){
	if(!array_) return;
	
	for(int i=0; i<arraySize_; i++){
		delete array_[i];
	}
	delete[] array_;
	delete prototypePtr_;

	array_ = 0;
	prototypePtr_ = 0;
	arraySize_ = 0;
}