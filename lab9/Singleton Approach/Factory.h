#pragma once

class TestObj{
	//Private fields:
	char *string_;
public:
	//Public constructors:
	//Creates an object initialised with string "domniemany"
	TestObj();
	//Creates an object initialised with string argument
	TestObj(const char *string);
	//Copying constructor
	TestObj(const TestObj& testObj);

	//Returns pointer to string
	const char *name() const;

	//Destructor
	//Frees string_
	~TestObj();
};

class Factory{
	//Singleton instance:
	static Factory *instance;
	//Private fields:
	//Holds an array of pointers to TestObj objects
	TestObj **array_;
	//Holds pointer to the created prototype. Otherwise it's equal to 0
	TestObj *prototypePtr_;
	//Holds array_ size
	int arraySize_;

	//Static private methods:
	//Singleton initializator
	static void ifNoInstanceCreate();
	//Private methods:
	//Increases array_ size by 1
	void increaseArraySize();
public:
	//Public constructors:
	//Creates a Factory object initialized with zeros
	Factory();
	//Static public methods:
	//Creates an TestObj object initialised with default constructor
	//and saves it to an array of pointers.
	//If prototype exists, creates an TestObj object initialised with
	//copying constructor (which uses the prototype)
	static TestObj *produce();
	//Creates an TestObj object initialised with copying
	//constructor (which uses an testObj arg)
	static TestObj *prototype(const TestObj& testObj);
	//Calls singleton destructor and deletes its instance
	static void erase();
	//Destructor
	//Frees memory allocated for TestObj objects and fills pointers with zeros
	~Factory();
};