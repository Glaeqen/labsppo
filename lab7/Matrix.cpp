#include <iostream>

#include "Vector.h"
#include "Matrix.h"

Matrix Matrix::diagonal(double value){
	return Matrix(Vector(value, 0, 0), Vector(0, value, 0), Vector(0, 0, value));
}

Matrix::Matrix(Vector v1, Vector v2, Vector v3){
	_vectors[0] = new Vector(v1.at(0), v1.at(1), v1.at(2));
	_vectors[1] = new Vector(v2.at(0), v2.at(1), v2.at(2));
	_vectors[2] = new Vector(v3.at(0), v3.at(1), v3.at(2));
}

Matrix& Matrix::set(int x, int y, double value){
	_vectors[x]->setCoord(y, value);
	return *this;
}

void Matrix::print() const{
	using std::cout;
	using std::endl;

	for(int x=0; x<3; x++){
		for(int y=0; y<3; y++){
			cout << _vectors[x]->at(y) << " ";
		}
		cout << endl;
	}
}

const Vector& Matrix::extractRow(int row) const{
	return *_vectors[row];
}

Vector Matrix::extractColumn(int column) const{
	double coordinates[3];

	for(int x=0; x<3; x++){
		coordinates[x] = _vectors[x]->at(column);
	}
	return Vector(coordinates[0], coordinates[1], coordinates[2]);
}

Matrix::~Matrix(){
	for(int i=0; i<3; i++){
		delete _vectors[i];
	}
}

