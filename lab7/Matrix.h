#pragma once

class Vector;

class Matrix{
	//Private fields:
	//Arrays of Vectors' pointers.
	Vector* _vectors[3];
public:
	//Static methods:
	//Makes diagonal matrix with value on its diagonal.
	static Matrix diagonal(double value);

	//Public constructors:
	//Creates Matrix object, initialized with 3 vectors.
	Matrix(Vector v1, Vector v2, Vector v3);

	//Non-const methods:
	//Sets specified value in matrix x, y coordinate.
	Matrix& set(int x, int y, double value);

	//Const methods:
	//Prints a matrix row after row.
	void print() const;
	//Returns constant reference to a vector which represent specified row.
	const Vector& extractRow(int row) const;
	//Returns Vector which is made of specified column in matrix.
	Vector extractColumn(int column) const;

	//Destructor:
	//Deletes vectors held by an array _vectors.
	~Matrix();
};