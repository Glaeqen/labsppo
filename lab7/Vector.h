#pragma once

class Vector{
	//Private fields:
	//Arrays of x, y, z coordinates.
	double _coordinates[3];
public:
	//Public constructors:
	//Creates Vector object, initialized with x, y, z coordinates.
	Vector(double x, double y, double z);

	// Non-const methods.
	// Set specified coordinate's value with specified value.
	void setCoord(int coordinate, double value);

	// Const methods.
	// Returns coordinate's value at specified coordinate.
	double at(int coordinate) const;

};