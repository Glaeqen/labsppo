#include "Vector.h"

Vector::Vector(double x, double y, double z){
	_coordinates[0] = x;
	_coordinates[1] = y;
	_coordinates[2] = z;
}

void Vector::setCoord(int coordinate, double value){
	_coordinates[coordinate] = value;
}

double Vector::at(int coordinate) const{
	return _coordinates[coordinate];
}