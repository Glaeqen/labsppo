#pragma once

class MapDist{
public:
	double latitude;
	double longitude;

	//Const methods:
	//Returns distance based on latitude and longitude in "this" MapDist object.
	double dist() const;
};

class MapPoint{
private:
	char* _name;
	double _latitude;
	double _longitude;

	//Private constructor, cause object shouldn't be initialized that way.
	MapPoint();

public:
	//Public constructors:
	//Constructor which initializes object with name, latitude and longitude.
	MapPoint(const char* name, double latitude, double longitude);

	//Static methods:
	//Static method which creates new MapPoint in the middle between mapPoint1 and mapPoint2.
	static MapPoint inTheMiddle(const MapPoint& mapPoint1, const MapPoint& mapPoint2, const char* newName);

	//Non-const methods:
	//Moves object with given lat/lon offset.
	void move(double latOffset, double lonOffset);

	//Const methods:
	//Returns const MapDist with distance between "this" mapPoint and mapPoint.
	MapDist distance(const MapPoint& mapPoint) const;
	//Returns reference to MapPoint (mapPoint1 or mapPoint2) which is the closest one to "this" mapPoint.
	const MapPoint& closestFrom(const MapPoint& mapPoint1, const MapPoint& mapPoint2) const;
	//Getters.
	double getLatitude() const;
	double getLongitude() const;
	//Prints name, latitude and longitude of "this" mapPoint.
	void print() const;
	//Prints name of "this" mapPoint.
	char* name() const;

	//Destructor:
	~MapPoint();
};