#include <iostream>
#include <cmath>
#include <cstring>

#include "MapPoint.h"

//MapDist
double MapDist::dist() const{
	double distance = sqrt(pow(latitude, 2) + pow(longitude, 2));
	return distance;
}

//MapPoint
MapPoint::MapPoint(const char* name, double latitude, double longitude){
	int nameLength = strlen(name) + 1;
	_name = new char[nameLength];

	strcpy(_name, name);
	_latitude = latitude;
	_longitude = longitude;
}

MapPoint MapPoint::inTheMiddle(const MapPoint& mapPoint1, const MapPoint& mapPoint2, const char* newName){
	double newLatitude = (mapPoint1.getLatitude() + mapPoint2.getLatitude())/2;
	double newLongitude = (mapPoint1.getLongitude() + mapPoint2.getLongitude())/2;
	return MapPoint (newName, newLatitude, newLongitude);
}

void MapPoint::move(double latOffset, double lonOffset){
	_latitude += latOffset;
	_longitude += lonOffset;
}

MapDist MapPoint::distance(const MapPoint& mapPoint)const{
	MapDist mapDist;
	mapDist.latitude = _latitude - mapPoint.getLatitude();
	mapDist.longitude = _longitude - mapPoint.getLongitude();
	return mapDist;
}

const MapPoint& MapPoint::closestFrom(const MapPoint& mapPoint1, const MapPoint& mapPoint2) const{
	MapDist mapDist1 = distance(mapPoint1);
	MapDist mapDist2 = distance(mapPoint2);
	if(mapDist1.dist() < mapDist2.dist())	return mapPoint1;
	else return mapPoint2;
}

double MapPoint::getLatitude() const{
	return _latitude;
}

double MapPoint::getLongitude() const{
	return _longitude;
}

void MapPoint::print() const{
	char lat = 'N';
	char lon = 'E';
	if(_latitude < 0)	lat = 'S';
	if(_longitude < 0) 	lon = 'W';

	std::cout << _name << " ";
	std::cout << fabs(_latitude) << lat << " ";
	std::cout << fabs(_longitude) << lon << std::endl;
}

char* MapPoint::name() const{
	return _name;
}

MapPoint::~MapPoint(){
	delete[] _name;
}